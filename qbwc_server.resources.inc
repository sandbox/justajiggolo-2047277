<?php
/**
 * Implements hook_services_resources() 
 */
function qbwc_server_services_resources() {
  /*@NOTE: These resoureces are defined by Intuit's Quickbooks Web Connector.
   *I have done my best to transfer documentation from the resources I have 
   *been able to gather from across the internet. My primary documentation for
   *developing these resources, and if you are reading this as a developer - 
   *what should also be one of your primary resources, has been the 
   *qbwc_proguide.pdf, which comes with the QBWC SDK. 
   */
  return array(
    'qbwc' => array(
      'actions' => array(
        //@todo: implement help functions using function descriptions from qbwc_proguide.pdf
        'authenticate' => array(
          'callback' => '_qbwc_authenticate',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('authenticate'),
          //@NOTE: not sure what effect '...append' has here
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'strUserName',
              'type' => 'string',
              'description' => t('Your %server login name.',
                array('%server', $_SERVER['SERVER_NAME'])),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('strUsername'),
            ),
            array(
              'name' => 'strPassword',
              'type' => 'string',
              'description' => t('Your %server password.',
                array('%server', $_SERVER['SERVER_NAME'])),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('strPassword'),
            ),
          ),
          'return' => 'struct',
        ),
        'clientVersion' => array(
          'callback' => '_qbwc_clientversion',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('clientversion'),
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'strVersion',
              'type' => 'string',
              'description' => t('The version of the QB web connector supplied 
                  in the web connector�s call to clientVersion.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('strVersion'),
            ),
          ),
          //see 'consolibyte' for return type
          //@link http://wiki.consolibyte.com/wiki/doku.php/quickbooks_web_connector @endlink
          'return' => 'string',
          //'help' => 'some function',
        ),
        'closeConnection' => array(
          'callback' => '_qbwc_closeconnection',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('closeconnection'),
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'ticket',
              'type' => 'string',
              'description' => t('The ticket from the web connector. This is 
                  the session token your web service returned to the web 
                  connector�s authenticate call, as the first element of the
                  returned string array.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('ticket'),
              ),
            ),
          'return' => 'string',
          ),
        'connectionError' => array(
          'callback' => '_qbwc_connectionerror',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('connectionerror'),
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'ticket',
              'type' => 'string',
              'description' => t('The ticket from the web connector. This is
                  the session token your web service returned to the web
                  connector�s authenticate call, as the first element of the
                  returned string array.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('ticket'),
              ),
            array(
              'name' => 'hresult',
              'type' => 'string',
              'description' => t('The HRESULT (in HEX) from the exception 
                  thrown by the request processor.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('hresult'),
              ),
            array(
              'name' => 'message',
              'type' => 'string',
              'description' => t('The error message that accompanies the 
                  HRESULT from the request processor.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('message'),
              ),
            ),
          //see 'consolibyte' for return type
          //@link http://wiki.consolibyte.com/wiki/doku.php/quickbooks_web_connector @endlink
          'return' => 'string',
          //'help' => 'some function',
          ),
        //@todo: Interactive Mode: getInteractiveURL
        'getLastError' => array(
          'callback' => '_qbwc_getlasterror',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('getlasterror'),
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'ticket',
              'type' => 'string',
              'description' => t('The ticket from the web connector. This is 
                  the session token your web service returned to the web 
                  connector�s authenticate call, as the first element of the 
                  returned string array.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('ticket'),
              ),
            ),
          'return' => 'string',
          ),
        //@todo: interactive Mode: interactiveDone
        //@todo: Interactive Mode: interactiveRejected
        'receiveResponseXML' => array(
          'callback' => '_qbwc_receiveresponsexml',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('receiveresponsexml'),
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'ticket',
              'type' => 'string',
              'description' => t('The ticket from the web connector. This is 
                  the session token your web service returned to the web 
                  connector�s authenticate call, as the first element of the 
                  returned string array.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('ticket'),
              ),
            array(
              'name' => 'response',
              'type' => 'string',
              'description' => t('Contains the qbXML response from QuickBooks 
                  or qbposXML response from QuickBooks POS.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('response'),
              ),
            array(
              'name' => 'hresult',
              'type' => 'string',
              'description' => t('The hresult and message could be returned as 
                  a result of certain errors that could occur when QuickBooks 
                  or QuickBooks POS sends requests is to the 
                  QuickBooks/QuickBooks POS request processor via the 
                  ProcessRequest call. If this call to the request processor
                  resulted in an error (exception) instead of a response, then 
                  the web connector will return the corresponding HRESULT and 
                  its text message in the hresult and message parameters. If no 
                  such error occurred, hresult and message will be empty 
                  strings.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('hresult'),
              ),
            array(
              'name' => 'message',
              'type' => 'string',
              'description' => t('The error message.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('See above under hresult.'),
              ),
            ),
          'return' => 'int',
          ),
        'sendRequestXML' => array(
          'callback' => '_qbwc_sendrequestxml',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('sendrequestxml'),
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'ticket',
              'type' => 'string',
              'description' => t('The ticket from the web connector. This is 
                  the session token your web service returned to the web 
                  connector�s authenticate call, as the first element of the 
                  returned string array'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('ticket'),
              ),
            array(
              'name' => 'strHCPResponse',
              'type' => 'string',
              'description' => t('Only for the first sendRequestXML call in a 
                  data exchange session will this parameter contains response 
                  data from a HostQuery, a CompanyQuery, and a 
                  PreferencesQuery request. This data is provided at the 
                  outset of a data exchange because it is normally useful for 
                  a web service to have this data. In the ensuing data 
                  exchange session, subsequent sendRequestXML calls from the 
                  web processor do not contain this data, (only an empty 
                  string is supplied) as it is assumed your web service 
                  already has it for the session.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('strHCPResponse'),
              ),
            array(
              'name' => 'strCompanyFileName',
              'type' => 'string',
              'description' => t('The company file being used in the current 
                  exchange.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('strCompanyFileName'),
              ),
            array(
              'name' => 'qbXMLCountry',
              'type' => 'string',
              'description' => t('The country version of QuickBooks or 
                  QuickBooks POS product being used to access the company. For 
                  example, US, CA (Canada), or UK.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('qbXMLCountry'),
              ),
            array(
              'name' => 'qbXMLMajorVers',
              'type' => 'int',
              'description' => t('The major version number (corresponding to 
                  the qbXML or qbposXML spec level) of the request processor 
                  being used. For example, the major number of the request 
                  processor released to support qbXML spec 6.0 would be �6�.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('qbXMLMajorVers'),
              ),
            array(
              'name' => 'qbXMLMinorVers',
              'type' => 'int',
              'description' => t('The minor version number (corresponding to 
                  the qbXML or qbposXML spec level) of the request processor 
                  being used. For example, the major number of the request 
                  processor released to support qbXML spec 6.0 would be �0�.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('qbXMLMinorVers'),
              ),
            ),
          'return' => 'string',
          ),
        'serverVersion' => array(
          'callback' => '_qbwc_serverversion',
          'access callback' => '_qbwc_endpoint_access',
          'access arguments' => array('serverversion'),
          'access arguments append' => TRUE,
          'file' => array('file' => 'methods.inc', 'module' => 'qbwc_server',),
          'key' => FALSE,
          //because of QBWC's predefined system, we need to authenticate our own way
          'auth' => FALSE,
          'args' => array(
            array(
              'name' => 'tersion',
              'type' => 'string',
              'description' => t('The ticket from the web connector. This is 
                  the session token your web service returned to the web 
                  connector�s authenticate call, as the first element of the 
                  returned string array.'),
              //'optional' => FALSE,
              //'signed' => FALSE,
              'title' => t('tersion'),
            ),
          ),
          //see 'consolibyte' for return type
          //@link http://wiki.consolibyte.com/wiki/doku.php/quickbooks_web_connector @endlink
          'return' => 'string',
          //'help' => 'some function',
        ),
        ),
      ),
  );
}

/**
 * Implements hook_services_resources_alter
 * 
 * Allows alteration of the services_resources array.
 *
 * @param array $resources
 *   The combined array of resource definitions from hook_services_resources.
 * @param array $endpoint
 *   An array describing the endpoint that resources are being built for.
 */
function qbwc_server_services_resources_alter(&$resources, &$endpoint) {
  if($endpoint->server == 'qbwc_server') {
    module_load_include('inc', 'qbwc_server');
    $resources = _qbwc_filter_resources($endpoint, $resources);
  }
}

/**
 * Control access to a QBWC_server endpoint's resources/functionality
 * 
 * @param unknown $op
 * @param string $qbwc
 * @param string $account
 * @return boolean
 */
function _qbwc_endpoint_access($op) {
  $return = FALSE;
  $args = func_get_args();
  GLOBAL $user;
  $info =  services_server_info_object();
  $endpoint = services_endpoint_load($info->endpoint);
  
  switch ($op) {
    case 'authenticate' :
      if(isset($args[1][0]->strUserName)
        && isset($args[1][0]->strPassword)) {
          //validate by username and password
          //call is to authenticate, where a token will be set
          //for now, grant access to authenticate call
          $return = TRUE;
        }
      break;
    case 'clientversion' :
      if(isset($args[1][0]->strVersion)) {
        $return = TRUE;
      }
      break;
    case 'closeconnection' :
      if(isset($args[1][0]->ticket)) {
        $return = TRUE;
      }
      break;
    case 'connectionerror' :
      if(isset($args[1][0]->ticket)
      && isset($args[1][0]->hresult)
      && isset($args[1][0]->message)) {
        $return = TRUE;
      }
      break;
    case 'getlasterror' :
      if(isset($args[1][0]->ticket)) {
        $return = TRUE;
      }
      break;
    case 'receiveresponsexml' :
      if(isset($args[1][0]->ticket)
      && isset($args[1][0]->response)
      && isset($args[1][0]->hresult)
      && isset($args[1][0]->message)) {
        $return = TRUE;
      }
      break;
    case 'sendrequestxml' :
      if(isset($args[1][0]->ticket)
      && isset($args[1][0]->strHCPResponse)
      && isset($args[1][0]->strCompanyFileName)
      && isset($args[1][0]->qbXMLCountry)
      && isset($args[1][0]->qbXMLMajorVers)
      && isset($args[1][0]->qbXMLMinorVers)) {
        $return = TRUE;
      }
      break;
    case 'serverversion' :
      //no args required - 
      $return = TRUE;
      break;
    default :
//       if(isset($args[1][0]->strToken)) {
//         $return = _qbwc_endpoint_authenticate_token($endpoint, $args[1][0]->strToken);
//       }
      watchdog('qbwc_server', t('call to authenticate %op failed', array('%op' => $op)));
  }
  return $return;
}

function _qbwc_endpoint_authenticate_token($endpoint, $token) {
  $args = func_get_args();
  return TRUE;
}