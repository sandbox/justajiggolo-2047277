<?php

module_load_include('inc', 'qbwc_server');

/**
 * Implements hook_form().
 */
function _qbwc_admin_settings_form($form = array(), &$form_state, $vars = null) {
  
  //@todo: make button to reset settings to default - at least for development
  $vars = _qbwc_server_default_settings();
  $form['qbwc_main_form'] = array(
    '#title' => t('Main Settings'),
    '#type' => 'vertical_tabs',
    '#weight' => -99,
    );
  $form['qbwc_default_settings'] = array(
    '#title' => t('QBWC Admin Settings'),
    '#description' => t('These settings are used by the QBWC Server module'),
    '#type' => 'fieldset',
    '#collapsible' => true,
    '#collapsed' => false,
    '#group' => 'qbwc_main_form',
    '#tree' => true,
    );
  $form['qbwc_default_settings']['QBWC_log_level'] = array (
    '#title' => t('QBWC module log level'),
    '#description' => t('Set the logging level of the qbwc_server module'),
    '#type' => 'select',
    '#options' => log_levels(),
    '#default_value' => variable_get('QBWC_log_level', 
        $vars['QBWC_log_level']),
    );
  $form['qbwc_default_settings']['QBWC_server_log_file'] = array(
    '#title' => t('QBWC Server Log File'),
    '#description' => t('The file name (path included) of the log file'),
    '#type' => 'textfield',
    '#default_value' => variable_get('QBWC_server_log_file', ''),
    );
  $form['qbwc_default_settings']['QBWC_minimum_frequency'] = array(
    '#title' => t('Minimum time between update requests'),
    '#description' => t('Set the minimum time allowed between requests, so
        the server doesn\'t get flooded. This setting is used as the minimum 
        when creating a QBWC endpoint, and therefore goes into the .qwc file.
        However, once that file is downloaded, it can be edited, and 
        potentially flood the server, anyway. This setting is also used to 
        ensure that if a request <em>is</em> made too frequently, then an error
        is returned.'),
    '#type' => 'textfield',
    '#default_value' => variable_get('QBWC_minimum_frequency', 600),
    );
  $form['qbwc_default_settings']['QBWC_endpoints_per_user'] = array(
    '#title' => t('Maximum number of endpoints per user'),
    '#description' => t('Set to 0 for unlimited'),
    '#type' => 'textfield',
    '#default_value' => variable_get('QBWC_endpointsper_user', 1),
    );
  //get all the default variables that have not been included yet
  $form['qbwc_default_settings']['unaccounted'] = array(
    '#title' => t('Unaccounted for'),
    '#decription' => t('These settings are added generically. If the correct'
      . ' type is not entered, things could break. Therefore - if ANY settings'
      . ' are in this field, it may mean that this module is not fully updated'
      . '. Use these settings with caution'),
    '#type' => 'fieldset',
    '#collapsible' => false,
    );

  foreach($vars as $key => $var) {
    if(!isset($form['qbwc_default_settings']["{$key}"])) {
      $form['qbwc_default_settings']['unaccounted']["{$key}"] = array(
        '#title' => $key,
        '#type' => 'textfield',
        '#attributes' => array (
          'class' => array('i-need-maintanence', $key),
          ),
        //'#class' => 'i-need-maintanence',
        '#default_value' => variable_get($key, $var),
        );
     }
   }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Update Settings',
    );
  foreach(module_invoke_all('qbwc_admin_settings_form', $vars) as $module => $elements) {
    //@todo: this needs to be tested
    $form[$module] = array(
      '#title' => t('%module Admin Settings', array('%module', $module)),
      '#description' => t('These settings are used by the %module module',
          array('%module', $module)),
      '#type' => 'fieldset',
      '#collapsible' => true,
      '#collapsed' => false,
      '#group' => 'qbwc_main_form',
      '#tree' => true,
    );
    foreach($elements as $element_key => $element) {
      $form[$module][$element_key] = $element;
    }
  }
  
  drupal_alter('qbwc_settings_form', $args);
  return $form;
}

/**
 * Implements hook_validate().
 */
// function _qwc_admin_settings_form_validate($form, &$form_state) {
//   //don't really need this just yet, but will eventually
// }

/**
 * Implements hook_submit()
 *
 * @param unknown $form
 * @param unknown $form_state
 */
function _qbwc_admin_settings_form_submit($form, &$form_state) {
  //if($op == 'Update Settings')
  $default_vars = _qbwc_server_default_settings();
  foreach($form_state['values']['qbwc_default_settings'] as $var => $val) {
    //$form_state['values'] has a lot more than just MY values - filter!
    if(isset($default_vars[$var]) && isset($val)){
      variable_set($var, $val);
    }
    //artifact: I accidentally added all kinds of crazy crap!
    //@todo: remove for production - leave for development!
//     elseif(!isset($default_vars[$var]) && variable_get($var, false)) {
//       //this item should not be in the variables table!
//       variable_del($var);
//     }
  }
  $args = array($form, &$form_state);
  module_invoke_all('qbwc_admin_settings_form_submit', $args);
}

function _qwbc_endpoint_settings(&$form, $endpoint, $settings) {
  //@NOTE: $settings = $endpoint->server_settings
  //but changes made to $settings are not persistent...WTF?
  //DONT USE $settings!
  //instead use $values during hook_submit
  module_load_include('inc', 'qbwc_server');
  //@REGION Preprocess form
  $default = _qbwc_server_endpoint_defaults();
  global $user;
  $dirty = FALSE;
  if(!isset($endpoint->server_settings) || !is_array($endpoint->server_settings)) {
    $endpoint->server_settings = array();
    $dirty = TRUE;
  }
  if(count($endpoint->server_settings) == 0) {
    $endpoint->server_settings = qbwc_new_endpoint();
    $dirty = TRUE;
  }
//@NOTE: endpoint->authentication is no longer necessary
// if(!isset($endpoint->authentication) 
//     || count($endpoint->authentication) == 0
//     || $endpoint->authentication === FALSE) {
//   $endpoint->authentication = array('qbwc_server' => 'qbwc_server',);
//   $dirty = TRUE;
// }
  if(!isset($endpoint->resources) 
      || count($endpoint->resources) == 0
      || $endpoint->resources === FALSE) {
    $endpoint->resources = qbwc_new_resources();
    $dirty = TRUE;
  }
  if(!isset($endpoint->server_settings['password']) 
      || $endpoint->server_settings['password'] == '') {
    $endpoint->enabled = FALSE;
    drupal_set_message(t('QBWC server %endpoint has been disabled until a 
        password is set', array('%endpoint' => $endpoint->name)));
  }
  if($dirty) {
    _qbwc_load_dependencies();
    services_endpoint_save($endpoint);
  }
  //@ENDREGION
  
  //@todo: make this into working vertical tabs
  $form['qbwc_server_endpoint_settings_form'] = array(
//     '#title' => t('QWC file Settings'),
    '#type' => 'vertical_tabs',
    '#weight' => -99,
    );
  
  //@REGION QWC Settings Tab
  $form['qwc_settings_tab'] = array(
    '#title' => t('QWC Settings'),
    '#description' => t('These settings are used to configure the .qwc file used by the QBWC'),
    '#type' => 'fieldset',
    '#collapsible' => true,
    '#collapsed' => false,
    '#group' => 'qbwc_server_endpoint_settings_form',
//     '#tree' => true,
    );
  
  $form['qwc_settings_tab']['title'] = array(
    '#type' => 'item',
    '#title' => isset($endpoint->path) ? $endpoint->path : $endpoint->name,
    '#weight' => -5,
    '#group' => 'qbwc_server_endpoint_settings_form',
  );
  
  $form['qwc_settings_tab']['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#default_value' => isset($endpoint->server_settings['description']) ? $endpoint->server_settings['description'] : '',
    '#maxlength' => 255,
    '#required' => FALSE,
    //'#weight' => -4,
    '#group' => 'qbwc_server_endpoint_settings_form',
  );
  
  //@todo: allow browse for file, but no need to actually upload it
  $form['qwc_settings_tab']['file'] = array(
    '#type' => 'textfield',
    '#title' => t('Quickbooks filename'),
    '#description' => t('Include the full path, or leave blank to use the 
        currently open Quickbooks file'),
    '#default_value' => isset($endpoint->server_settings['file']) ? $endpoint->server_settings['file'] : '',
    '#group' => 'qbwc_server_endpoint_settings_form',
    );
  
  $form['qwc_settings_tab']['username'] = array(
    '#type' => 'textfield',
    '#title' => t('QBWC Username'),
    '#description' => t('This username does not have to be the same as your'
        . ' username for this site, nor does it correspond to a Quickbooks'
        . ' username. This is for correspondence between the QBWC \'client\' and'
        . ' this server, as specified by the \'QBWC\' you are creating now.'),
    '#default_value' => isset($endpoint->server_settings['username']) ? $endpoint->server_settings['username'] : $user->name,
    '#maxlength' => 60, //@see schema['qbwc']
    '#required' => TRUE,
    '#group' => 'qbwc_server_endpoint_settings_form',
  );
  
  $form['qwc_settings_tab']['password'] = array(
    '#type' => 'password_confirm',
    '#size' => 25,
    '#description' => t('This password SHOULD NOT be the same as your'
        . ' password for this site, nor does it correspond to a Quickbooks'
        . ' password. This is for correspondence between the QBWC \'client\' and'
        . ' this server, as specified by the \'QBWC\' you are creating now.'),
  );
  
  //@todo: use ajax-ness to have an (disabled) edit field for fileid, ownerid
  //@todo: create button to regenerate fileid & ownerid
//   if(isset($endpoint->server_settings['fileid'])) {
    $form['qwc_settings_tab']['fileid'] = array(
      '#type' => 'item',
      '#title' => t('File ID'),
      '#markup' => isset($endpoint->server_settings['fileid']) ? $endpoint->server_settings['fileid'] : random_guid(),
      '#description' => 'This value is auto generated, and should not be changed unless you know what you are doing!'
    );
//   }
  
//   if(isset($endpoint->server_settings['ownerid'])) {
    $form['qwc_settings_tab']['ownerid'] = array(
      '#type' => 'item',
      '#title' => t('Owner ID'),
      '#markup' => isset($endpoint->server_settings['ownerid']) ? $endpoint->server_settings['ownerid'] : random_guid(),
      '#description' => 'This value is auto generated, and should not be changed unless you know what you are doing!'
    );
//   }
  
  $form['qwc_settings_tab']['qbtype'] = array(
    '#type' => 'radios',
    '#title' => t('QB Type'),
    '#options' => array(
      'QBFS' => 'QBFS',
      'QBPOS' => 'QBPOS',
    ),
    '#default_value' => isset($endpoint->server_settings['qbtype']) ? $endpoint->server_settings['qbtype'] : $default['qb_file_type'],
    '#description' => t('If you don\'t have a specific reason to change this
        then don\'t.'),
  );
  
  $form['qwc_settings_tab']['readonly'] = array(
    '#type' => 'checkbox',
    '#title' => t('Read-only'),
    '#default_value' => isset($endpoint->server_settings['readonly']) ? $endpoint->server_settings['readonly'] : $default['readonly'],
    '#description' => t('Use this to prevent specific connections from
        changing data'),
  );
  
  $form['qwc_settings_tab']['frequency'] = array(
    '#type' => 'textfield',
    '#title' => t('Allowed QBWC client query (seconds)'),
    '#description' => t('The QBWC can be told only to check for updates at'
        . ' certain intervals. The system Admin can force a \'minimum\' time'
        . ' between requests, which may override the value you use here.'),
    '#default_value' => isset($endpoint->server_settings['frequency']) ? $endpoint->server_settings['frequency'] : $default['update_frequency'],
    '#size' => 25,
    '#maxlength' => 25, //@todo: resize this, that is potentially a long time!
  );
  
  module_load_include('inc', 'qbwc_server');
  $s = _qbwc_server_default_settings()['QBWC_use_ssl_cert'];
  $path = "http{$s}://" . $_SERVER['HTTP_HOST'] . '/' . $endpoint->path . '/?qwc';
  $form['qwc_settings_tab']['get_qwc'] = array(
    '#type' => 'item',
    '#title' => l(t('Download qwc file'), $path),
    );
  
//   $form['qwc_settings_tab']['empty'] = array(
//     '#type' => 'item',
//     '#prefix' => '<div id="empty-placeholder">',
//     '#suffix' => '</div>',
//     );
  
//   $form['qwc_settings_tab']['get_qwc'] = array(
//     '#type' => 'button',
//     '#value' => t('Download qwc'),
//     '#executes_submit_callback' => FALSE,
//     '#ajax' => array(
//       'callback' => 'qbwc_server_qwc_output_ajax_wrapper',
// //       'path' => 'admin/structure/services/list/' . $endpoint->path . 
// //         '/server/qwc',
// //       'prevent' => 'mousedown',
//       'wrapper' => 'empty-placeholder',
//       ),
//     );
  //@ENDREGION

  //@REGION General Settings tab
  $form['qbwc_server_endpoint_general_settings_tab'] = array(
    '#title' => t('QBWC Endpoint Settings'),
    '#description' => t('These settings are used to configure the behavior of the QBWC server endpoint'),
    '#type' => 'fieldset',
    '#collapsible' => true,
    '#collapsed' => false,
    '#group' => 'qbwc_server_endpoint_settings_form',
    '#tree' => true,
  );
  $form['qbwc_server_endpoint_general_settings_tab']['token_life'] = array(
    '#type' => 'textfield',
    '#title' => t('For how long should a session be valid (seconds)'),
    '#description' => t('A session will expire after it is inactive for this 
        time period, and the QBWC will have to re-authenicate'),
    '#default_value' => isset($endpoint->server_settings['token_life']) ? $endpoint->server_settings['token_life'] : $default['token_life'],
    '#size' => 25,
    '#maxlength' => 25,
    );
  $form['qbwc_server_endpoint_general_settings_tab']['qbwc_exclusive'] = array(
    '#type' => 'checkbox',
    '#title' => t('QBWC Exclusive'),
    '#default_value' => isset($endpoint->server_settings['qbwc_exclusive']) ? $endpoint->server_settings['qbwc_exclusive'] : $default['qbwc_exclusive'],
    '#description' => t('Prevent this endpoint from allowing requests that
        aren\'t from Intuit\'s QBWC'),
  );
  //@ENDREGION
  
  //@REGION Endpoint log settings tab
  //@todo: make buttons to download log, clear log...
  $form['qbwc_server_endpoint_log_settings_tab'] = array(
    '#title' => t('QBWC Endpoint Log Settings'),
    '#description' => t('These settings are used to configure QBWC endpoint Logging'),
    '#type' => 'fieldset',
    '#collapsible' => true,
    '#collapsed' => false,
    '#group' => 'qbwc_server_endpoint_settings_form',
    '#tree' => true,
  );
  $form['qbwc_server_endpoint_log_settings_tab']['log_level'] = array (
    '#type' => 'select',
    '#title' => t('QBWC endpoint log level'),
    '#description' => t('Set the logging level of this QBWC endpoint'),
    '#options' => log_levels(),
    '#default_value' => isset($endpoint->server_settings['log_level']) ? $endpoint->server_settings['log_level'] : $default['endpoint_log_level']
    );
  $form['qbwc_server_endpoint_log_settings_tab']['log_file'] = array(
    '#type' => 'textfield',
    '#title' => t('file to output log to'),
    '#description' => t('"log_file" needs a description'),
    '#default_value' => isset($endpoint->server_settings['log_file']) ? $endpoint->server_settings['log_file'] : '',
  );
  //@ENDREGION
  
  //@todo: need buttons for user to download and .wsdl
  $form['actions'] = array(
    '#type' => 'container',
    '#attributes' => array('class' => array('form-actions')),
    '#weight' => 400,
  );
  
  $form['#validate'][] = '_qbwc_endpoint_settings_validate';
}
//@todo: validate does not get called, does element-wise validate get called?
function _qbwc_endpoint_settings_validate($endpoint, &$values) {
  $noop = 0;
  //validate that freq > admin freq
  //@todo: add element-wise validation, if that gets called
}
/**
 * This is not an ordinary form submit!
 * 
 * @see services_edit_form_endpoint_server_submit()...
 * Instead of saving $values into the endpoint here, services_module throws all
 * $values into $endpoint->server_settings blob - so weed out unwanted $values! 
 */
function _qbwc_endpoint_settings_submit($endpoint, &$values) {
  //@todo: why doesn't validate get called?
  //@todo: this try block needs to be refactored
  if(isset($_POST['op']) && $_POST['op'] == 'Save' && count($values) > 0) {
    try {
      //consolidate the arrays
      $new_values = array();
      foreach($values as $tab => $array) {
        $new_values += $array;
      }
      $values = $new_values;
      //@region - should be done by validate function
      //make sure anything in $endpoint->server_settings persists
      foreach($endpoint->server_settings as $setting => $value) {
        if(!isset($values[$setting])) {
          $values[$setting] = $value;
        }
      }
      if((!isset($values['password']) || $values['password'] == '')
          && isset($endpoint->server_settings['password'])
          && strlen($endpoint->server_settings['password'])  >= 0) {
        $values['password'] = $endpoint->server_settings['password'];
      }
      if(!isset($values['fileid'])) {
        $values['fileid'] = '{' . random_guid() . '}';
      }
      if(!isset($values['ownerid'])) {
        $values['ownerid'] = '{' . random_guid() . '}';
      }
      //@end region
      if(isset($values['title'])) {
        unset($values['title']);
      }
      if(isset($values['qbwc_server__qbwc_server_endpoint_settings_form__active_tab'])) {
        unset($values['qbwc_server__qbwc_server_endpoint_settings_form__active_tab']);
      }
      if(isset($values['get_qwc'])) {
        unset($values['get_qwc']);
      }
      //@REGION Password handling
      //if password exists
      if (isset($values['password']) && $values['password'] != '') {
        //if password is not the one already set (and hashed) then hash and set
        if($values['password'] != $endpoint->server_settings['password']) {
          // Allow alternate password hashing schemes.
          //         require_once DRUPAL_ROOT . '/' . variable_get('password_inc', 'includes/password.inc');
          module_load_include('inc', 'qbwc_server', 'qbwc_server.auth');
          $values['password'] = _qbwc_server_hash_password(trim($values['password']));
          // Abort if the hashing failed and returned FALSE.
        }
        //if error occured during hash
        if (!$values['password']) {
          watchdog('qbwc_server', 'Failed to save password');
          drupal_set_message(t('Failed to save password'));
          $endpoint->enabled = FALSE;
          drupal_set_message(t('QBWC server %endpoint has been disabled until a
              password is set', array('%endpoint' => $endpoint->name)));
        }
        //else everything is good!
        else {
          //since password is set/hashed, make sure it is enabled
          $endpoint->enabled = TRUE;
        }
      }
      else {
        //password is not set, disable endpoint
        $endpoint->enabled = FALSE;
        drupal_set_message(t('QBWC server %endpoint has been disabled until a 
            password is set', array('%endpoint' => $endpoint->name)));
      }   
      //@ENDREGION
    }
    catch(Exception $e) {
      //@todo: log - 'Failed to save QWC'
      drupal_set_message(t('Failed to save QBWC configuration', 'error'));
    }
  }
  return $values;
}