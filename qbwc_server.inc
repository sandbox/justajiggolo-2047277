<?php
/**
 * @file
 * Helpful functions for qbwc_server
 */



// CONSTANTS
if(!defined('LOG_DEVELOP')) define('LOG_DEVELOP', 4);
if(!defined('LOG_DEBUG')) define('LOG_DEBUG', 3);
if(!defined('LOG_VERBOSE')) define('LOG_VERBOSE', 2);
if(!defined('LOG_NORMAL')) define('LOG_NORMAL', 1);
if(!defined('LOG_NONE')) define('LOG_NONE', 0);
// END CONSTANTS

function log_levels($level = null) {
  //yeah, a bit redundant, but may help later
  $array = array(
      LOG_NONE => t('None'),
      LOG_NORMAL => t('Normal'),
      LOG_VERBOSE => t('Verbose'),
      LOG_DEBUG => t('Debug'),
      LOG_DEVELOP => t('Develop'),
      );
  return $level ? $array[$level] : $array;
}

/**
 * Used to track/maintain the admin settings for this module
 *
 * @return array of setting_name => default_value pairs
 */
function _qbwc_server_default_settings() {
  //It might be interesting to have nested arrays to return 'possible' values
  //  and a description of the intended use of each variable
  //For now, this is small enough and there is only one developer
  return array(
      'QBWC_log_level' => LOG_DEVELOP,
      'QBWC_server_log_file' => drupal_get_path('module', 'qbwc_server')
      . '/qbwc_server.log',
      'QBWC_minimum_frequency' => 600,
      'QBWC_endpoints_per_user' => 1,
      'QBWC_use_ssl_cert' => FALSE,
      'QBWC_minimum_client_version' => "2.1.0.24",
      'QBWC_support_page_nid' => FALSE,
  );
}

/**
 * Used to track/maintain the default values for the qbwc_server entity
 *
 * @return array of property_name => default_value pairs
 */
function _qbwc_server_endpoint_defaults() {
  return array(
      'qb_file_type' => 'QBFS',
      'readonly' => FALSE,
      'qbwc_exclusive' => TRUE,
      'update_frequency' => 600,
      'endpoint_log_level' => LOG_DEVELOP,
      'endpoint_log_file' => drupal_get_path('module', 'qbwc_server')
        . '/qbwc_endpoint.log',
      'token_life' => 600,
  );
}

/**
 * Log qbwc information to an output file
 * 
 * @param string $file
 *   The filename (including path)
 * @param string $module
 *   The name of the module responsible for loggint the information
 * @param string $msg
 *   The message to be logged (pre-translated)
 * @param mixed $args
 *   The arguments to be dumped (context)
 * 
 * @return
 *   the return value of 'file_put_contents'
 *   @see file_put_contents
 */
function _qbwc_log($file, $module, $msg, $args) {
  if(isset($file) && file_exists($file)) {  
    $timestamp = time();
    $data = "$timestamp - $module: $msg " . PHP_EOL
      . var_export($args);
    return file_put_contents($file, $data, FILE_APPEND);
  }
  return FALSE;
}

/**
 * Generates a 32 hex character "GUID" with hiphens
 * 
 * The format is "HHHHHHHH-HHHH-HHHH-HHHH-HHHHHHHHHHHH" 
 * @return string
 */
function random_guid() {
  //8-4-4-4-12
  $num = md5(mt_srand());
  $n1 = substr($num, 0, 8);
  $n2 = substr($num, 8, 4);
  $n3 = substr($num, 12, 4);
  $n4 = substr($num, 16, 4);
  $n5 = substr($num, 20, 12);
  
  return "{$n1}-{$n2}-{$n3}-{$n4}-{$n5}";  
}

/**
 * Create an array of default settings for a qbwc endpoint
 * 
 * @return array
 *   keyed by setting name
 */
function qbwc_new_endpoint_default_settings() {
  GLOBAL $user;
  module_load_include('inc', 'qbwc_server');
//   module_load_include('inc', 'qbwc_server', 'consolibyte');
  $defaults = _qbwc_server_endpoint_defaults();
  //ensure default freq is not invalid (by being too low)
  $frequency = $defaults['update_frequency'];
  $freq = variable_get('QBWC_minimum_frequency', FALSE);
  if( $freq && $freq > $frequency) {
    $frequency = $freq;
  } 
  else {
    $freq = 0;
  }
  $settings = array(
    'uid' => $user->uid,
    //title is part of endpoint
    'password' => '',
    'description' => '',
    'username' => $user->name,
    'fileid' => '{' . random_guid() . '}',
    'ownerid' => '{' . random_guid() . '}',
    'qbtype' => $defaults['qb_file_type'],
    'filepath' => '',
    'readonly' => $defaults['readonly'],
    'frequency' => $frequency,
    'accessed' => 0,
    'log_level' => $defaults['endpoint_log_level'], 
    'log_file' => $defaults['endpoint_log_file'],
    );
  return $settings;
}

/**
 * Create/enable an array of valid (required) QBWC endpoint resources
 * 
 * The resources listed by hook_services_resources are mandatory for a QBWC
 * endpoint to function properly. Since Intuit has but the carriage before the 
 * horse (created the QBWC client, with no 'standard' server) this function 
 * ensures that the required resources are enabled, even if disabled in the
 * admin/structure/services/list/%endpoint/resources UI
 * 
 * @return array of enabled endpoint resources
 */
function qbwc_new_resources() {
  $return = array();
  foreach(qbwc_server_services_resources() as $name => $group) {
    foreach($group as $type => $methods) {
      foreach($methods as $method_name => $method_array) {
        $return[$name][$type][$method_name]['enabled'] = '1';
      }
    }
  }
  return $return;
}

/**
 * Loads the dependencies necessary to save QBWC endpoint modifications
 */
function _qbwc_load_dependencies() {
  //@region STUPIDITY
  //@todo: is there a better way of doing this? this is stupid!
  module_load_include('module', 'serivces');
  module_load_include('module', 'ctools');
  ctools_include('export');
  //@endregion
}

/**
 * return a list of stock messages for the qbwc_server module
 * 
 * @param string $key
 *   The message array key
 * @param boolean $auto_set
 *   Automatically send the message to drupal_set_message
 * @param string $args
 *   The args to send to translation with the selected message
 * @return
 *   if no key is given, returns the whole l
 *   if a key is given but auto_set is FALSE, return the specified item
 *   if a key is given and auto_set is TRUE, return drupal_set_message
 */
function _qbwc_messages($key = NULL, $auto_set = TRUE, $args = NULL) {
  $msgs = array();
  $msgs['PW_FAIL_HASH'] = array(
    'msg' => t('Failed to properly hash the password: %additional', $args),
    'type' => 'warning',
    );
  $msgs['QBWC_CONFIG_FAIL'] = array(
    'msg' => t('Could not load the QBWC configuration for endpoint %endpoint.',
        //array('%endpoint' => $endpoint->name)),
        $args),
    'type' => 'warning',
    );
  $msgs['PW_ENDPOINT_DISABLED'] = array(
    'msg' => t('Endpoint %endpoint has been disabled until a password is set.',
        $args),
    'type' => 'status',
    );
  //@todo: add any additional messages...
  
  if(!isset($key)) {
    return $msg;
  }
  elseif(isset($msgs[$key])) {
    if($auto_set) {
      return drupal_set_message($msg[$key]['msg'], $msgs[$key]['type']);
    }
    else {
      return $msgs[$key];
    }
  }
}

/**
 * Remove all resoureces that are not specific to QBWC
 * 
 * The resources listed by hook_services_resources are mandatory for a QBWC
 * endpoint to function properly. Since Intuit has but the carriage before the 
 * horse (created the QBWC client, with no 'standard' server) this function 
 * ensures that there are no additional resources, even if enabled in the
 * admin/structure/services/list/%endpoint/resources UI. The reason for this
 * is primarily security related, since authentication is performed outside 
 * drupal.
 * 
 * @param $endpoint
 *   The QBWC endpoint (or endpoint name as string)
 * @param $resources
 *   An array of available endpoint resources
 * @return array
 *   An array of QBWC specific resources
 */
function _qbwc_filter_resources($endpoint, $resources) {
  //REMOVE ALL NON-QBWC RESOURCES,
  if(is_string($endpoint)){
    $endpoint = services_endpoint_load($endpoint);
  }
  if(!isset($endpoint->server_settings['qbwc_exclusive'])
      || $endpoint->server_settings['qbwc_exclusive']) {
    foreach($resources as $resource_key => $resource_type) {
      //@NOTE: this is a stupid way to do this, but strpos === FALSE is not working
      //if(!(strpos($resource_key, 'qbwc') === FALSE) ) {
      if(substr($resource_key, 0, 4) != 'qbwc') {
        unset($resources[$resource_key]);
      }
    }
  }
  return $resources;
}