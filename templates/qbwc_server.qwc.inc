<?php
if(isset($endpoint)) {
  $appname = $endpoint->path;
  $appid = '';
  $appurl = '';
  $appdesc = '';
  if(isset($endpoint->server_settings)
      && isset($endpoint->server_settings['description'])) {
    $appsesc = $endpoint->server_settings['description'];
  }
  $appsupport = '';
  $username = $endpoint->server_settings['username'];
  $ownerid = $endpoint->server_settings['ownerid'];
  $fileid = $endpoint->server_settings['fileid'];
  $qbtype = $endpoint->server_settings['qbtype'];
  $minutes = (int) ($endpoint->server_settings['frequency'] / 60);
  $readonly = $endpoint->server_settings['readonly'];
  
  $qwc =  <<<QWCOUTPUT
<?xml version="1.0" encoding='UTF-8' ?>
<QBWCXML>
  <AppName>{$appname}</AppName>
  <AppID>{$appid}</AppID>
  <AppURL>{$appurl}</AppURL>
  <AppDescription>{$appdesc}</AppDescription>
  <AppSupport>{$appsupport}</AppSupport>
  <UserName>{$username}</UserName>
  <OwnerID>{$ownerid}</OwnerID>
  <FileID>{$fileid}</FileID>
  <QBType>{$qbtype}</QBType>
  <Scheduler>
  	<RunEveryNMinutes>{$minutes}</RunEveryNMinutes>
  </Scheduler>
  <IsReadOnly>{$readonly}</IsReadOnly>
</QBWCXML>
QWCOUTPUT;
}