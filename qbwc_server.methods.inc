<?php
/**
 * Prompts the web service to authenticate the specified user and specify the 
 * company to be used in the session.
 * 
 * @NOTE: not sure why parameters are delivered as an array, when defined
 *   properly in qbwc_server.resources - qbwc_server_services_resources()
 *   
 * @param unknown $strUsername
 * @param unknown $strPassword
 * @return QBWC specific array
 *   @see qbwc_proguide.pdf, p.62 - can't remember where I dl from
 *   this does not mention that ret[0] should be the validating token
 *   """""""""""""""""""""""""""""""""""""""""""""""""""'
 *   Return Value
 *   Your callback must return A string array with 4 possible elements. In this returned string
 *   array:
 *   � The first element contains either NONE or NVU (invalid user) or BUSY., or empty
 *   string, or a string that is the QB company file name. If your web service returns an
 *   empty string or any other string that is NOT nvu or none, or busy, that string will be
 *   used as the qbCompanyFileName parameter in the web connector�s BeginSession call
 *   to QuickBooks.
 *   � The second element enables the web service to postpone the update process. The value
 *   in this parameter determines the number of seconds by which the update will be
 *   postponed. For example if authRet[2]=60, the WebConnector will postpone the update
 *   process by 60 seconds. That is, the current update process is discontinued and will
 *   resume after 60 seconds.
 *   � The third element (optional) sets the lower limit for the Every_Min parameter (this
 *   parameter determines the interval the scheduler uses to run the updates when autorun is
 *   enabled). For example, if the third element =300 seconds, suppose a tries to set
 *   Every_Min=2 min using the UI of the WebConnector instance. In this case the result
 *   would be a popup that informs the user that the lower limit for this parameter is 300
 *   seconds.and the Web Connector will automatically set the Every_Min parameter to 5
 *   min (300 seconds).
 *   � The fourth element (optional) contains the number of seconds to be used as the
 *   MinimumRunEveryNSeconds time.
 *   """"""""""""""""""""""""""""""""""""""""""""""""""""""
 */
// function _qbwc_authenticate($strUsername, $strPassword) {
function _qbwc_authenticate() {
  $args = func_get_args();
  $name = $args[0]->strUserName;
  $pass = $args[0]->strPassword;
  $ret = array();
  
  if(!isset($name) || !isset($pass)) {
    //@todo: log error
//     $xml = 'error authenticating: incorrect parameters supplied - requires a 
//         (string) username and (string) password.';
    $ret = array('', 'nvu');
  }
  module_load_include('inc', 'qbwc_server');
  //check when the last access was, and make sure frequency is not violated
  $info =  services_server_info_object();
  $endpoint = services_endpoint_load($info->endpoint);
  $accessed = $endpoint->server_settings['accessed'];
  $freq = $endpoint->server_settings['frequency'];
  if( $freq < variable_get('QBWC_minimum_frequency')) {
    $freq = variable_get('QBWC_minimum_frequency');
  }
  $elapsed = time() - $accessed;
  module_load_include('inc', 'qbwc_server', 'qbwc_server.auth');
  if($elapsed < $freq) {
    $wait = (int) (($freq - $elapsed) / 60);
//     $xml = "authentication failed because you are trying to access the server 
//         to frequently. Please wait ~{$wait} more minutes before you try again";
    $ret = array('', 'none');
  }
  
  //check if name/pass are ok
  elseif(_qbwc_server_check_endpoint_password($name, $pass)) {
    //generate token and save to endpoint->server_settings['session_id']
//     services_set_server_info('session_id', session_id());
    $endpoint->server_settings['token'] = session_id();
    //update endpoint->server_settings['accessed'] time
//     services_set_server_info('accessed', time());
    $endpoint->server_settings['accessed'] = time();
    _qbwc_load_dependencies();
    services_endpoint_save($endpoint);
    //zeroth param - token
    $ret[] = $endpoint->server_settings['token'];
    //First param - filename or none or busy
    //@todo: make function for queuecount
    $queuecount = 0;
    if(isset($endpoint->server_settings['file']) && queuecount > 0) {
      $first = $endpoint->server_settings['file'];
    }
    elseif(queuecount == 0) {
      $first = 'NONE';
    }
    else {
      $first = '';
    }
    $ret[] = $first;
    //second param - postpone update?
    //@todo: make hook to allow modules\settings to alter this
    $ret[] = '';
    //third param - lower limit ? wtf is intuit thinking?
    $ret[] = '';
    //fourth param - MinimumRunEveryNSeconds - seriously wtf?
    $ret[] = '';
    
  }
  else {
    //@todo: log error
//     $xml = "authentication failed - invalid username or password";
    $ret = array('', 'nvu');
  }
  $ret = array('authenticateResult' => $ret);
  return $ret;
}

/**
 * Optional callback allows the web service to evaluate the current web 
 * connector version and react to it. Not currently required to support 
 * backward compatibility but strongly recommended.

 * @see qbwc_proguide.pdf, p.62 - can't remember where I dl from
 * 
 * Parameters
 * strVersion The version of the QB web connector supplied in the web
 * connector�s call to clientVersion.
 * 
 * Return Value
 * A string telling the web connector what to do next. Supply one of the following strings:
 * � Specify an empty string or Null if you want the web connector to proceed with the
 * update.
 * � Specify a text string that begins with the characters "W:" if you want the web connector
 * to display a WARNING dialog prompting the user to continue with the update or cancel
 * it. The text string after the �W:� will be displayed in the warning dialog.
 * � Specify a text string that begins with the characters "E:" if you want the web connector
 * to cancel the update and display an ERROR dialog. The text string after the �E:� will
 * be displayed in the error dialog. The user will have to download a new version of the
 * web connector to continue with the update.
 * � Supply a value of O: (O as in Okay, not zero, followed by the QBWC version supported
 * by the web service). For example O:2.0. This tells the user that the server expects a
 * newer version of QBWC than the user currently has but also tells the user which
 * version is needed.
 */
function _qbwc_clientversion($strVersion) {
  $args = func_get_args();
  $version = $args[0]->strVersion;
  $ret = array('clientVersionResult' => '');
  $min = variable_get('QBWC_minimum_client_version', "2.1.0.24");
  $expver = explode(".", $version);
  $expmin = explode(".", $min);
  foreach($expmin as $index => $value) {
    if(!isset($expver[$index])) {
      $expver[$index] = 0;
    }
    if($expver[$index] < $value) {
      $ret = array('clientVersionResult' => "O:{$min}");
    }
  }
  //@todo: check for warnings and errors, proceed accordingly
  return $ret;
}

/**
 * Tells your web service that the web connector is finished with the update 
 * session.
 * 
 * @param unknown $ticket
 * 
 * @return
 * Return Value
 * Specify a string that you want the web connector to display to the user 
 * showing the status of the web service action on behalf of your user. This 
 * string will be displayed in the web connector UI in the status column.
 * 
 * Usage
 * When the update with the web service is completed, the web connector will 
 * notify the web service that it is done with the session it started by 
 * calling closeConnection. fifth of the six required methods for your web 
 * service:
 * 
 * Sample Code (C-sharp)
 * The following code sample is taken from the QB SDK sample program 
 * WCWebService. It doesn�t do anything very interesting, just returns an �OK� 
 * message.
 */
function _qbwc_closeconnection($ticket) {
  $args = func_get_args();
  //@todo: return array('closeConnectionResult' => (string) $ret)
}

/**
 * 
 * @param unknown $ticket
 * @param unknown $hresult
 * @param unknown $message
 */
function _qbwc_connectionerror($ticket, $hresult, $message) {
  $args = func_get_args();
  
  //@todo: log error
  //@todo: send error to drupal set message
  //@todo: watchdog

  $ret = 'DONE';
  $override = drupal_alter('QBWC_Connection_Error', $ret, $args);
  if(count($override) > 0) {
    $ret = $array_shift($override);
  }
  return array('connectionErrorResult' => (string) $ret);
}

/**
 * 
 * @param unknown $ticket
 */
function _qbwc_getlasterror($ticket) {
  $args = func_get_args();
  //@todo: return array('getLastErrorResult' => (string) $ret)
}

/**
 * 
 * @param unknown $ticket
 * @param unknown $response
 * @param unknown $hresult
 * @param unknown $message
 */
function _qbwc_receiveresponsexml($ticket, $response, $hresult, $message) {
  $args = func_get_args();
  //@todo: return array('receiveResponseXMLResult' => (int) $ret)
}

/**
 * 
 * @param unknown $ticket
 * @param unknown $strHCPResponse
 * @param unknown $strCompanyFileName
 * @param unknown $qbXMLCountry
 * @param unknown $qbXMLMajorVers
 * @param unknown $qbXMLMinorVers
 */
function _qbwc_sendrequestxml($ticket, $strHCPResponse, $strCompanyFileName,
    $qbXMLCountry, $qbXMLMajorVers, $qbXMLMinorVers) {
  $args = func_get_args();
  //@todo: return array('sendRequestXMLResult' => (string) $ret)
}

/**
 * 
 * @param unknown $ticket
 * @return multitype:unknown
 */
function _qbwc_serverversion($ticket) {
  $args = func_get_args();
  $module_vers = system_get_info('module', 'qbwc_server')['version'];
  return array('serverVersionResult' => $module_vers);
}







