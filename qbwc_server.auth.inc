<?php
/**
 * @file
 * contains f'ns for managing endpoint passwords and authenticating requests
 */

module_load_include('inc', 'qbwc_server');

/**
 * Authenticates an incoming QBWC request
 *
 * The functionality of this was directly copied from services.auth
 * 
 * @NOTE: This function was intended to be called from the services 
 * authentication hook via hook_authentication_info(). However, because of 
 * the QBWC client developed by intuit, there is a more practical way of 
 * authenticating, that does not require this function
 * @todo: remove this function for production
 */
function _qbwc_server_authenticatation() {
  $arg = func_get_args();
  $module = $arg[0];
  $resource = $arg[1];
  $fn_call_args = $arg[2];
  $info =  services_server_info_object();
  $endpoint = services_endpoint_load($info->endpoint);
  $validated = FALSE;
  GLOBAL $user;
  
  if($resource['callback'] == '_qbwc_authenticate') {
    //validate username / password
    //@todo: make sure the request does not violate the server's frequency rules
    $validated = _qbwc_server_check_endpoint_password($fn_call_args[0], $fn_call_args[1]);
    if($validated) {
      services_set_server_info('session_id', session_id());
      services_set_server_info('accessed', time());
      _qbwc_load_dependencies();
      services_endpoint_save($endpoint);
    }
  }
  else {
    /*@NOTE: this should validate by ticket 
     *this has been left unfinished because validation via this method was 
     *abandoned. it should be noted though, that if this function is to be 
     *used, by any other module, that this else stub should be finished.
     */
    $noop = 0;
  }
  if($validated
      && isset($endpoint->server_settings['uid'])) {
    $user = user_load($endpoint->server_settings['uid']);
  }
  else {
    $user = NULL;
  }
  
  return isset($user->uid) && $user->uid != 0 ? $user->uid : FALSE;
}

/**
 * Hash a password, using a similar mechanism as the user module
 * 
 * This function is just a wrapper for user module's 'user_hash_password()
 * 
 * @see user_hash_password()
 *
 * @param unknown $password
 * @param number $count_log2
 */
function _qbwc_server_hash_password($password, $count_log2 = 0) {
  require_once(DRUPAL_ROOT . '/includes/password.inc');
  return user_hash_password($password, $count_log2);
}

/**
 * Check if an incoming Services (QBWC) request has valid credentials
 * @see user_check_password()
 *
 * @param unknown $account
 * @param unknown $password
 * 
 * @return bool
 *   TRUE if username and password match according to endpoint->server_settings
 */
function _qbwc_server_check_endpoint_password($username, $password) {
  $info =  services_server_info_object();
  $endpoint = services_endpoint_load($info->endpoint);
  
  if(!isset($endpoint->server_settings['password']) 
      || $endpoint->server_settings['password'] == '') {
    drupal_set_message(t('Could not load the Qbwc configuration for
      endpoint %endpoint', array('%endpoint' => $endpoint->name)),
      'warning');
    //@todo: send to logs
  }
  $stored_hash = $endpoint->server_settings['password'];

  require_once(DRUPAL_ROOT . '/includes/password.inc');
  $type = substr($stored_hash, 0, 3);
  //@NOTE: shouldn't need this switch, leaving it in for potential migration
  //purposes
  switch ($type) {
    case '$S$':
      // A normal Drupal 7 password using sha512.
      $hash = _password_crypt('sha512', $password, $stored_hash);
      break;
    case '$H$':
      // phpBB3 uses "$H$" for the same thing as "$P$".
    case '$P$':
      // A phpass password generated using md5.  This is an
      // imported password or from an earlier Drupal version.
      $hash = _password_crypt('md5', $password, $stored_hash);
      break;
    default:
      return FALSE;
  }
  return ($hash && $stored_hash == $hash 
      && isset($endpoint->server_settings['username']) 
      && $endpoint->server_settings['username'] == $username);
}